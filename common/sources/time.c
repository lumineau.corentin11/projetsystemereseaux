
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>

typedef struct Time Time;
struct Time
{
    int heure;
    int minutes;
};

/**
 * TrainTimeToNumber(char *t)
 * 
 * Passe l'heure d'un train d'un String en Struct Time
 * 
 * return Time
 **/

Time TrainTimeToNumber(char *t)
{
    Time res;
    char *timeParts[2];
    timeParts[0] = strtok(t, ":");
    timeParts[1] = strtok(NULL, ":");
    res.heure = atoi(timeParts[0]);
    res.minutes = atoi(timeParts[1]);
    return res;
}

/**
 * ASupB(Time HA, Time HB)
 * 
 * Renvoie 1 si HA supérieur à HB sinon renvoie 0
 * 
 * return int
 **/

int ASupB(Time HA, Time HB)
{
    int res;
    if ((HA.heure > HB.heure) || ((HA.heure == HB.heure) && (HA.minutes > HB.minutes)))
    {
        res = 1;
    }
    else
    {
        res = 0;
    }
    return res;
}

/**
 * AInfB(Time HA, Time HB)
 * 
 * Renvoie 1 si HA inférieur à HB sinon renvoie 0
 * 
 * return int
 **/

int AInfB(Time HA, Time HB)
{
    int res;
    if ((HA.heure < HB.heure) || ((HA.heure == HB.heure) && (HA.minutes < HB.minutes)))
    {
        res = 1;
    }
    else
    {
        res = 0;
    }
    return res;
}

/**
 * ASupEqB(Time HA, Time HB)
 * 
 * Renvoie 1 si HA supérieur ou egale à HB sinon renvoie 0
 * 
 * return int
 **/

int ASupEqB(Time HA, Time HB)
{
    int res;
    if ((HA.heure > HB.heure) || ((HA.heure == HB.heure) && (HA.minutes >= HB.minutes)))
    {
        res = 1;
    }
    else
    {
        res = 0;
    }
    return res;
}

/**
 * AInfEqB(Time HA, Time HB)
 * 
 * Renvoie 1 si HA inférieur ou egal à HB sinon renvoie 0
 * 
 * return int
 **/

int AInfEqB(Time HA, Time HB)
{
    int res;
    if ((HA.heure < HB.heure) || ((HA.heure == HB.heure) && (HA.minutes <= HB.minutes)))
    {
        res = 1;
    }
    else
    {
        res = 0;
    }
    return res;
}

/**
 * AInfEqB(Time HA, Time HB)
 * 
 * Renvoie 1 si HA  egal à HB sinon renvoie 0
 * 
 * return int
 **/

int AEqB(Time HA, Time HB)
{
    int res;
    if ((HA.heure == HB.heure) && (HA.minutes == HB.minutes))
    {
        res = 1;
    }
    else
    {
        res = 0;
    }
    return res;
}

/**
 * duree(Time t1, Time t2)
 * 
 * Renvoie le temps qu'il y entre entre t1 et t2
 * 
 * return Time
 **/

Time duree(Time t1, Time t2)
{
    Time res;
    int resInMinutes;
    //passe le time en int de minutes pour les soustraires
    int minutesTot1 = t1.heure * 60 + t1.minutes;
    int minutesTot2 = t2.heure * 60 + t2.minutes;
    resInMinutes = minutesTot2 - minutesTot1;
    //puis retransforme le temps en Time
    res.heure = resInMinutes / 60;
    res.minutes = resInMinutes % 60;
    return res;
}