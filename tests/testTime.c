#include "../common/sources/time.c"

void timeTest()
{
    char test[] = "6:10";

    Time t = TrainTimeToNumber(test);
    char test2[] = "6:10";

    printf("il est %d : %d Heure attendu %s \n", t.heure, t.minutes, test2);

    Time a;
    a.heure = 9;
    a.minutes = 15;

    Time b;
    b.heure = 9;
    b.minutes = 15;

    Time c;
    c.heure = 10;
    c.minutes = 59;

    int res;
    res = AEqB(a, b);
    printf("AEqB : %d  Valeur attendu : 1 \n", res);
    res = AEqB(a, c);
    printf("AEqB : %d Valeur attendu : 0\n", res);

    res = ASupB(c, a);
    printf("ASupB : %d Valeur attendu : 1 \n", res);
    res = ASupB(a, c);
    printf("ASupB : %d Valeur attendu : 0\n", res);

    res = ASupEqB(c, a);
    printf("ASupEqB : %d Valeur attendu : 1 \n", res);
    res = ASupEqB(a, b);
    printf("ASupEqB : %d Valeur attendu : 1 \n", res);
    res = ASupEqB(a, c);
    printf("ASupEqB : %d Valeur attendu : 0\n", res);

    res = AInfB(a, c);
    printf("AInfB : %d Valeur attendu : 1 \n", res);
    res = AInfB(c, a);
    printf("AInfB : %d Valeur attendu : 0\n", res);

    res = AInfEqB(a, c);
    printf("AInfEqB : %d Valeur attendu : 1\n", res);
    res = AInfEqB(a, b);
    printf("AInfEqB : %d Valeur attendu : 1\n", res);
    res = AInfEqB(c, a);
    printf("AInfEqB : %d Valeur attendu : 0\n", res);

    Time d = duree(a, c);
    printf("le trajet dur : %d : %d \n", d.heure, d.minutes);
}

int main(int nbArgs, char *arg[])
{
    timeTest();
}