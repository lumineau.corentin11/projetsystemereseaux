#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "../headers/train.h"
#include "../headers/time.h"
#include "../headers/indexation.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#define nbTrainMAX 40
#define TAILLE_MAX 50000

Train remplirStructure(char ligne[]) //retourne un objet de type train avec toutes les informations fournis par ligne[] qui correspond à un train
{
    Train train;

    char *stock[100];
    char *l2 = strtok(ligne, ";"); //decoupage as ";"
    int i = 0;
    while (l2 != NULL)
    {
        stock[i] = l2;
        l2 = strtok(NULL, ";");
        i++;
    }

    train.numero = atoi(stock[0]); //affectation des valeurs à l'objet train
    train.villeD = strdup(stock[1]);
    
    train.villeA = strdup(stock[2]);
    train.horaireD = TrainTimeToNumber(stock[3]);
    train.horaireA = TrainTimeToNumber(stock[4]);
    train.prix = atof(stock[5]);
    if (i > 6)
    {
        train.modif = strdup(stock[6]);
        if (strcmp(train.modif, "REDUC\n") == 0)
        {
            train.prix = train.prix * 0.8;
        }
        else if (strcmp(train.modif, "SUPPL\n") == 0)
        {
            train.prix = train.prix * 1.1;
        }
    }

    return train;
}

Train *indexation() //recupère le fichier de trains en format csv et le transforme en tableau de train
{
    FILE *fp;
    char str[TAILLE_MAX];
    char *filename = "../tests/train.txt";
    int nbTrains = 0;
    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        printf("Could not open file %s\n", filename);
    }
    while (fgets(str, TAILLE_MAX, fp) != NULL) //count the number of train into file
    {
        nbTrains++;
    }
    rewind(fp);
    Train *arrayTrain = (Train *)malloc((sizeof(Train)) * (nbTrains + 1));
    fgets(str, TAILLE_MAX, fp);
    arrayTrain[0] = remplirStructure(str);
    for (int i = 1; i < nbTrains; i++) //rempli le tableau de train
    {

        fgets(str, TAILLE_MAX, fp);

        arrayTrain[i] = remplirStructure(str);
    }
    fclose(fp);
    arrayTrain[nbTrains] = trainMarqueFin(); // ajout du train marque de fin
    return arrayTrain;
}