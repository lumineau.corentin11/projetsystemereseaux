#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#define MAX 100
#define MAXLONGREQ 100

/**
 * void uiC(char train[])
 * 
 * Affiche de manière structuré une ligne de resultat de requête
 **/

void uiC(char train[])
{

	char *res[8];
	int i = 0;

	// découpe le résultat envoyé par le serveur
	res[i] = strtok(train, ";");
	i++;
	while (i < 8)
	{
		res[i] = strtok(NULL, ";");
		i++;
	}

	//Afficher de manière jolie le résultat
	printf(" \033[1;4;36mNo");
	printf("\033[0;0;36m %s", res[0]);
	printf("\033[0;0;36m | ");
	printf("\033[1;4;33mDépart:");
	printf("\033[0;0;33m %s à %s", res[1], res[3]);
	printf("\033[0;0;36m | ");
	printf("\033[1;4;33mArrivé:");
	printf("\033[0;0;36m %s à %s ", res[2], res[4]);
	printf("\033[1;4;33mDurée:");
	printf("\033[0;0;36m %s", res[6]);
	printf("\033[0;0;36m | ");
	printf("\033[1;4;33mPrix:");

	//Vérifie l'existant de reduction ou de suppléments ou non pour changer la couleur du prix
	if (res[7] != NULL)
	{
		if (strcmp(res[7], "REDUC\n") == 0)
		{
			printf("\033[0;0;32m %.5s", res[5]);
		}
		else if (strcmp(res[7], "SUPPL\n") == 0)
		{
			printf("\033[0;0;31m %.5s", res[5]);
		}
	}
	else
	{
		printf("\033[0;0;36m %.5s", res[5]);
	}
	printf("\n");
}

int main(int nbArgs, char *arg[])
{
	//Initialisation de la connexion avec le serveur
	int chaussette;
	chaussette = socket(AF_INET, SOCK_STREAM, 0);

	struct hostent *host;
	host = gethostbyname("localhost");

	struct sockaddr_in server;

	memcpy(&server.sin_addr.s_addr, host->h_addr_list[0], sizeof(server.sin_addr.s_addr));
	server.sin_family = AF_INET;
	server.sin_port = htons(4269);

	connect(chaussette, (struct sockaddr *)&server, sizeof(server));

	char tampon[MAX];
	char req[MAXLONGREQ];
	char choix[MAXLONGREQ];
	int nbRes;

	printf("\033[0;32m---------------------------------------------------------------------------- \n");
	printf("\033[0;32m|");
	printf("\033[0;32m            Programming in C, it's dangerous to go Alone \n");
	printf("\033[0;32m|");
	printf("\033[0;32m                     	Take your socks\n");
	printf("\033[0;32m|");
	printf("\033[0;32m\n");
	printf("\033[0;32m|");
	printf("\033[0;32m            		Powered by SNCBABYLONE \n");
	printf("\033[0;32m---------------------------------------------------------------------------- \n");

	printf("Entrez votre requete :\033[0m \n");
	//recupération de la requete
	fgets(req, MAXLONGREQ, stdin);
	write(chaussette, req, strlen(req) + 1);
	read(chaussette, tampon, MAX);
	if (strcmp(tampon, "-1") == 0)
	{
		//traitement de l'erreur lors de la réception du code erreur
		printf("Erreur dans la requete\n");
		exit(-1);
	}
	else if (strcmp(tampon, "1") == 0)
	{
		//traitement de l'erreur lors de la réception du code de validation de la requete
		printf("\033[0;32mMeilleur prix (\033[1;1;31m1\033[0;32m), Trajet le plus court (\033[1;1;31m2\033[0;32m), Tous les trajets (\033[1;1;31m0\033[0;32m)\n");
		printf("Entrez \033[1;1;31m1\033[0;32m, \033[1;1;31m2\033[0;32m ou \033[1;1;31m0\033[0m\n");
		fgets(choix, MAXLONGREQ, stdin);
		write(chaussette, choix, strlen(choix) + 1);

		//traitement de l'erreur lors de la réception du code de choix
		switch (atoi(choix))
		{
		case 1:
			read(chaussette, tampon, MAX);
			printf("\033[0;32m ---------------------------------------------------------------------------- \n");
			printf("\033[0;32m 1.");
			uiC(tampon);
			printf("\033[0;32m ---------------------------------------------------------------------------- \n");
			break;
		case 2:
			read(chaussette, tampon, MAX);
			printf("\033[0;32m ---------------------------------------------------------------------------- \n");
			printf("\033[0;32m 1.");
			uiC(tampon);
			printf("\033[0;32m ---------------------------------------------------------------------------- \n");
			break;
		case 0:
			//récupération du nombre de résultats pour echanger le bon nombre de fois avec les serveur
			read(chaussette, tampon, MAX);
			printf("%s", tampon);
			nbRes = atoi(tampon);
			int i = 0;
			while (i < nbRes)
			{
				read(chaussette, tampon, MAX);
				printf("\033[0;32m ---------------------------------------------------------------------------- \n");
				printf("\033[0;32m %d.", i + 1);

				uiC(tampon);
				i++;
			}
			printf("\033[0;32m ---------------------------------------------------------------------------- \n");
			break;
		default:
			break;
		}
	}
	else
	{
		if (strcmp(tampon, "Aucun resultat") != 0)
		{
			printf("\033[0;32m ---------------------------------------------------------------------------- \n");
			printf("\033[0;32m 1.");
			uiC(tampon);
			printf("\033[0;32m ---------------------------------------------------------------------------- \n");
		}
		else
		{
			printf("\033[0;31m%s\033[0;32m\n", tampon);
		}
	}
	return chaussette;
}