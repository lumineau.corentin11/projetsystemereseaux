#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include "../headers/time.h"
#include "../headers/train.h"
#define nbTrainMAX 40
#define TAILLE_MAX 50000

Train trainMarqueFin() //renvoie un train dont le numero est -1, permettant aux autres fonctions de parcourir un tableau de train grâce à cette marque de fin
{
	Train train1;
	train1.numero = -1;
	train1.villeD = "marquefinVilleDepart";
	train1.villeA = "marquefinVilleArrive";
	train1.horaireD.heure = 0;
	train1.horaireD.minutes = 0;
	train1.horaireA.heure = 0;
	train1.horaireA.minutes = 0;
	train1.prix = 0.0;
	train1.modif = "";
	return train1;
}

int nbTrains(Train *res) //retourne le nombre de Train d'un tableau, le train marque de fin n'est pas compté
{
	int nbtrain = 0;
	while (res[nbtrain].numero != -1)
	{
		nbtrain++;
	}
	return nbtrain;
}

Train *reqVilles(Train *t, char *villeD, char *villeA) //retourne les Trains allant d'une ville de départ et une ville d'arrivée précises
{
	int i = 0;
	int nb = 0;

	while (t[i].numero != -1)
	{

		if ((strcmp(t[i].villeD, villeD) == 0) && (strcmp(t[i].villeA, villeA) == 0))
		{
			nb++;
		}
		i++;
	}
	Train *trainresult = (Train *)malloc((nb + 1) * sizeof(Train));
	i = 0;
	nb = 0;
	while (t[i].numero != -1) //remplissage du tableau à retourner
	{
		if ((strcmp(t[i].villeD, villeD) == 0) && (strcmp(t[i].villeA, villeA) == 0))
		{

			trainresult[nb] = t[i];

			nb++;
		}
		i++;
	}
	trainresult[nb] = trainMarqueFin();
	return trainresult;
}

Train *reqVillesDuree(Train *t, char *villeD, char *villeA, Time horaireD1, Time horaireD2) //retourne les Trains allant d'une ville de départ et une ville d'arrivée précises
{																							//l'heure de départ doit être comprise dans une tranche horaire donnée en paramètre
	int i = 0;
	int nb = 0;
	while (t[i].numero != -1) //première boucle pour connaître l'espace mémoire à allouer pour le tableau à retourner
	{
		if (((strcmp(t[i].villeD, villeD) == 0) && (strcmp(t[i].villeA, villeA) == 0)) && ((((ASupB(t[i].horaireD, horaireD1)) != 0) && ((AInfB(t[i].horaireD, horaireD2)) != 0))))
		{
			nb++;
		}
		i++;
	}
	Train *trainresult = (Train *)malloc((nb + 1) * sizeof(Train));
	i = 0;
	nb = 0;
	while (t[i].numero != -1) //remplissage du tableau à retourner
	{
		if (((strcmp(t[i].villeD, villeD) == 0) && (strcmp(t[i].villeA, villeA) == 0)) && ((((ASupB(t[i].horaireD, horaireD1)) != 0) && ((AInfB(t[i].horaireD, horaireD2)) != 0))))
		{
			trainresult[nb] = t[i];
			nb++;
		}
		i++;
	}
	trainresult[nb] = trainMarqueFin();
	return trainresult;
}

Train *reqVillesHeure(Train *t, char *villeD, char *villeA, Time horaireD1) //retourne un tableau contenant le Train respectant un horaire de départ donné
{																			//ou le premier Train possible à partir de l'horaire demandé
	Train *trainresult = (Train *)malloc(2 * sizeof(Train));
	int i = 0;
	while (t[i].numero != -1) //première boucle pour connaître l'espace mémoire à allouer pour le tableau à retourner
	{
		if (((strcmp(t[i].villeD, villeD) == 0) && (strcmp(t[i].villeA, villeA) == 0)) && (AEqB(t[i].horaireD, horaireD1)))
		{
			trainresult[0] = t[i];
			trainresult[1] = trainMarqueFin();
			return trainresult;
		}
		i++;
	}
	Train mintrain;
	i = 0;
	int nb = 0;
	mintrain.horaireD.heure = 24;
	mintrain.horaireD.minutes = 60;
	while (t[i].numero != -1) //remplissage du tableau à retourner
	{
		if (((strcmp(t[i].villeD, villeD) == 0) && (strcmp(t[i].villeA, villeA) == 0)) && (ASupB(t[i].horaireD, horaireD1)) && ((AInfB(t[i].horaireD, mintrain.horaireD)) != 0))
		{
			mintrain = t[i];
			nb++;
		}
		i++;
	}
	if (nb != 0)
	{
		trainresult[0] = mintrain;
		trainresult[1] = trainMarqueFin();
		return trainresult;
	}
	else
	{
		trainresult[0] = trainMarqueFin();
		return trainresult;
	}
}

Train trainMoinscher(Train *t) //renvoie le train dont le prix est le plus bas d'un tableau de train donné en paramètre
{
	int i = 0;
	Train trainMinPrix;
	trainMinPrix = t[0];
	while (t[i].numero != -1)
	{
		if (t[i].prix < trainMinPrix.prix)
		{
			trainMinPrix = t[i];
		}
		i++;
	}
	return trainMinPrix;
}

Train trainplusCourt(Train *t) //renvoie le train dont le temps entre l'horaire de départ et d'arrivée est le plus bas d'un tableau de train donné en paramètre
{

	int i = 0;
	Train traincourt;
	traincourt = t[0];
	while (t[i].numero != -1)
	{
		if ((AInfB(duree(t[i].horaireD, t[i].horaireA), duree(traincourt.horaireD, traincourt.horaireA))) != 0)
		{
			traincourt = t[i];
		}
		i++;
	}
	return traincourt;
}

char *trainToString(Train t) //retourne la chaine de caractères correspondant aux données d'un train
{
	char *string = malloc(TAILLE_MAX);
	Time tps = duree(t.horaireD, t.horaireA);
	if (strcmp(t.modif, "REDUC\n") == 0 || strcmp(t.modif, "SUPPL\n") == 0)
	{

		sprintf(string, "%d;%s;%s;%d:%d;%d:%d;%f;%d:%d;%s", t.numero, t.villeD, t.villeA, t.horaireD.heure, t.horaireD.minutes, t.horaireA.heure, t.horaireA.minutes, t.prix, tps.heure, tps.minutes, t.modif);
	}
	else
	{
		sprintf(string, "%d;%s;%s;%d:%d;%d:%d;%f;%d:%d", t.numero, t.villeD, t.villeA, t.horaireD.heure, t.horaireD.minutes, t.horaireA.heure, t.horaireA.minutes, t.prix, tps.heure, tps.minutes);
	}

	return string;
}
