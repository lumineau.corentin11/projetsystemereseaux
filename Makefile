#compile all *.c
all: 
	make commun
	make compileserveur
	make compileclient 
	
#launch server
serveur:
	cd server;make

#launch client
clients:
	cd client;make

#compile commom function
commun:
	cd common;make

#compile server
compileserveur:
	cd server;make server

#compile client
compileclient:
	cd client;make client

#clear all *.o
clean:
	cd server;make clean
	cd client;make clean
	cd common;make clean