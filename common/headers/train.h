#ifndef TRAIN_H_ /* Include guard */
#define TRAIN_H_
#include "time.h"
typedef struct Train Train;
struct Train
{
    int numero;
    char *villeD;
    char *villeA;
    Time horaireD;
    Time horaireA;
    double prix;
    char *modif;
};

int nbTrains(Train *res);
Train *reqVilles(Train *t, char *villeD, char *villeA);
Train *reqVillesHeure(Train *t, char *villeD, char *villeA, Time horaireD1);
Train *reqVillesDuree(Train *t, char *villeD, char *villeA, Time horaireD1, Time horaireD2);
Train trainMoinscher(Train *res);
Train trainplusCourt(Train *res);
char *trainToString(Train t);
Train trainMarqueFin();

#endif
