#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include "../../common/headers/indexation.h"
#include "../../common/headers/train.h"
#define MAX 50
#define MAXREQ 100
#define MAXREP 3

int main(int nbArgs, char *arg[])
{
	int chaussette, csock;
	//Ouverture de la socket
	chaussette = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in sin;
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(4269);
	bind(chaussette, (struct sockaddr *)&sin, sizeof(sin));
	listen(chaussette, 5);

	//Recuperation de tout les trains
	Train *AllTrain;
	AllTrain = indexation();
	int nbt = nbTrains(AllTrain);

	while (true)
	{
		csock = accept(chaussette, NULL, NULL);
		if (fork() == 0)
		{
			printf("Un client s'est connecté \n");

			char tampon[MAXREQ];

			read(csock, tampon, MAXREQ);

			//Decoupage des differents champ de la requete
			char *argReq[4];
			int i = 0;
			int nbArg = 0;
			argReq[0] = strtok(tampon, ";");
			while (argReq[i] != NULL)
			{
				i++;
				argReq[i] = strtok(NULL, ";");
				nbArg++;
			}

			Train *res = (Train *)malloc(sizeof(Train) * (nbt + 1));
			int nbRes = 0;
			char *erreurReq = "-1";

			switch (nbArg)
			{
			case 3: //Recuperation de tout les trajets d'une ville D a un ville A

				res = reqVilles(AllTrain, argReq[0], argReq[1]);
				nbRes = nbTrains(res);
				break;

			case 4: //Recuperation du premier trajet d'une ville D a un ville A a partir d'une heure h

				res = reqVillesHeure(AllTrain, argReq[0], argReq[1], TrainTimeToNumber(argReq[2]));
				nbRes = nbTrains(res);
				break;

			case 5: //Recuperation de tout les trajets d'une ville D a un ville A dans une plage horaire

				res = reqVillesDuree(AllTrain, argReq[0], argReq[1], TrainTimeToNumber(argReq[2]), TrainTimeToNumber(argReq[3]));
				nbRes = nbTrains(res);
				break;

			default: //Probleme dans la requete (pas le bon nombre d'argument)

				write(csock, erreurReq, strlen(erreurReq) + 1);
				nbRes = -1;
				break;
			}

			if (nbRes > 1)
			{ //Plus d'un resultat

				char *demande = "1";
				write(csock, demande, strlen(demande) + 1);
				char repDemande[MAXREP];
				read(csock, repDemande, MAXREP);
				int rep = atoi(repDemande);

				if (rep == 1)
				{ //Renvoie le trajet le moins cher
					Train moinsCher = trainMoinscher(res);
					char *moinsCherString = trainToString(moinsCher);
					write(csock, moinsCherString, strlen(moinsCherString) + 1);
				}
				else if (rep == 2)
				{ //Renvoie le trajet le plus court
					Train plusCourt = trainplusCourt(res);
					char *plusCourtString = trainToString(plusCourt);
					write(csock, plusCourtString, strlen(plusCourtString) + 1);
				}
				else
				{ //Renvoie tous les trajets
					char nbResChar[12];
					sprintf(nbResChar, "%d", nbRes);
					write(csock, nbResChar, strlen(nbResChar) + 1); //Envoie du nombre de resultats pour connaitre le nombre de read
					int i = 0;
					char *trainString;
					while (i < nbRes)
					{
						trainString = trainToString(res[i]);
						printf("%d : %s \n", i, trainString);
						write(csock, trainString, strlen(trainString) + 1);
						usleep(150000);
						i++;
					}
				}
			}
			else if (nbRes == 1)
			{ //Un seul resultat (pas de choix)
				char *resTrain = trainToString(res[0]);
				write(csock, resTrain, strlen(resTrain) + 1);
			}
			else if (nbRes == 0)
			{ //Aucun resultat
				char *noRes = "Aucun resultat";
				write(csock, noRes, strlen(noRes) + 1);
			}
			printf("Un client s'est déconnecté \n");
			shutdown(csock, 2);
		}
	}
	return csock;
}
