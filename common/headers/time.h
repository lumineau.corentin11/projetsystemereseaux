#ifndef TIME_H_ /* Include guard */
#define TIME_H_

typedef struct Time Time;
struct Time
{
    int heure;
    int minutes;
};
Time TrainTimeToNumber(char *t);
int ASupB(Time HA, Time HB);
int AInfB(Time HA, Time HB);
int ASupEqB(Time HA, Time HB);
int AInfEqB(Time HA, Time HB);
int AEqB(Time HA, Time HB);
Time duree(Time t1, Time t2);

#endif